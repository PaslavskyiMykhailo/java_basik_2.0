package Java_basic.homework06;

import Java_basic.homework06.first.Divide;
import Java_basic.homework06.first.Minus;
import Java_basic.homework06.first.Multiply;
import Java_basic.homework06.first.Plus;

public interface Numerable extends Plus, Minus, Divide, Multiply {

}
