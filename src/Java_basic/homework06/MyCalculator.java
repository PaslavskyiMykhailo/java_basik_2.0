package Java_basic.homework06;

public class MyCalculator implements Numerable {
    @Override
    public double divide(double a, double b) {
        return a / b;
    }

    @Override
    public double minus(double a, double b) {
        return a - b;
    }

    @Override
    public double multiply(double a, double b) {
        return a * b;
    }

    @Override
    public double plus(double a, double b) {
        return a + b;
    }

    public MyCalculator() {
    }

}
