package Java_basic.homework06;

public class Main {
    public static void main(String[] args) {
        MyCalculator calculator = new MyCalculator();

        System.out.println("плюс = " + calculator.plus(2, 15.3));
        System.out.println("мінус = " + calculator.minus(15.8, 10));
        System.out.println("ділення = " + calculator.divide(485, 147));
        System.out.println("множення = " + calculator.multiply(17, 65));

    }
}
