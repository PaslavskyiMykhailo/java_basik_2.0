package Java_basic.homework05;

public abstract class Weapon {

    private String weaponName ;
    private int hitDamage;


    public Weapon(String weaponName, int hitDamage) {
        this.weaponName = weaponName;
        this.hitDamage = hitDamage;
    }

    public Weapon() {
    }

    public String getWeaponName() {
        return weaponName;
    }

    public void setWeaponName(String weaponName) {
        this.weaponName = weaponName;
    }

    public int getHitDamage() {
        return hitDamage;
    }

    public void setHitDamage(int hitDamage) {
        this.hitDamage = hitDamage;
    }

    @Override
    public String toString() {
        return   weaponName +
                ", hitDamage= " + hitDamage ;
    }

    public abstract double damage();
}
