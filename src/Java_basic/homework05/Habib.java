package Java_basic.homework05;

public class Habib extends Warrior {

    public Habib(String name, int health, Weapon weapon) {
        super(name, health, weapon);
    }

    @Override
    public void strike() {
        System.out.println("Soldier " + getName() + " attack enemy with power : ");
    }

    @Override
    public boolean isDead() {
        if (getHealth() <= 0) return true ;
        else return false;
    }
}
