package Java_basic.homework05;

public class M16 extends Weapon{
    public M16(String weaponName, int hitDamage) {
        super(weaponName, hitDamage);
    }

    @Override
    public double damage() {
        return getHitDamage() * (0.5 + Math.random() * 2);
    }
}
