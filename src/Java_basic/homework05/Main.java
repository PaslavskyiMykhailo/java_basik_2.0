package Java_basic.homework05;

public class Main {
    public static void main(String[] args) {

        AK47 ak47 = new AK47("AK-47", 15);
        Ivan ivan = new Ivan("Ivan", 200, ak47);

        M16 m16 = new M16("M-16", 12);
        Jony jony = new Jony("Jony", 200, m16);

        PPSh ppSh = new PPSh("PPSh", 10);
        Habib habib = new Habib("Habib", 250, ppSh);

        War war = new War();
        war.war(ivan,jony,habib);

    }
}
