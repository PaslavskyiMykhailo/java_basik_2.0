package Java_basic.homework05;

public abstract class Warrior {

    private String name;
    private double health;
    private Weapon weapon ;

    public abstract void strike();

    public abstract boolean isDead();

    public Warrior(String name, int health, Weapon weapon) {
        this.name = name;
        this.health = health;
        this.weapon = weapon;
    }

    public Warrior() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        return "Warrior " + name +
                ", health= " + health +
                ", weapon= " + weapon ;
    }
}
