package Java_basic.homework05;

public class War {

    private double hit;

    public void war (Ivan ivan,Jony jony,Habib habib){

        do {

            System.out.println("Ivan attack jony");
            System.out.println(ivan.toString());
            System.out.println(jony.toString());
            ivan.strike();
            hit = ivan.getWeapon().damage();
            jony.setHealth(jony.getHealth() - hit);
            System.out.println(hit);
            System.out.println(jony.toString());
            System.out.println("------------||-------------");
            System.out.println();

            System.out.println("jony attack habib");
            System.out.println(jony.toString());
            System.out.println(habib.toString());
            jony.strike();
            hit = jony.getWeapon().damage();
            habib.setHealth(habib.getHealth() - hit);
            System.out.println(hit);
            System.out.println(habib.toString());
            System.out.println("------------||-------------");
            System.out.println();

            System.out.println("habib attack ivan");
            System.out.println(habib.toString());
            System.out.println(ivan.toString());
            habib.strike();
            hit = habib.getWeapon().damage();
            ivan.setHealth(ivan.getHealth() - hit);
            System.out.println(ivan.toString());
            System.out.println(habib.toString());
            System.out.println("------------||-------------");
            System.out.println();
        }
        while (!ivan.isDead() && !jony.isDead() || !jony.isDead() && !habib.isDead() || !habib.isDead() && !ivan.isDead());

        if (ivan.isDead() && jony.isDead()) System.out.println(" Habib win ");
        if (jony.isDead() && habib.isDead()) System.out.println(" Ivan win ");
        if (habib.isDead() && ivan.isDead()) System.out.println(" Jony win ");

    }
}
