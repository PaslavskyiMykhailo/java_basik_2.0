package Java_basic.homework02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // deposit();
        // massRoll();
    }

    /*Створити двовимірний масив, який буде мати 5 рядків і 7 стовпців і заповнити його рандомними(Math.random())
    цифрами з проміжку [-2, 75]*/

    private static void doubleMass (){

    }



    /*Користувач задає елементи масиву певного розміру (розмір масиву вибирає програміст).
    Ваше завдання вивести масив, який ввів користувач, а потім вивести масив у зворотньому порядку.*/

    private static void massRoll (){
        Scanner scanner = new Scanner(System.in);
        int [] mass = new int[10];
        for (int i = 0; i < mass.length; i++) {
            System.out.println("Enter number");
            mass[i]=scanner.nextInt();
        }
        System.out.println("Your mass are :");
        for (int i = 0; i < mass.length; i++) {
            System.out.println(mass[i]);
        }
        System.out.println("From end");
        for (int i = 9; i >=0 ; i--) {
            System.out.println(mass[i]);
        }
    }

    /*В банк поклали m - гривень під n-% відсотків річних. Яким буде вклад за K-років.
    Користувач вводить : m -суму грошей , n - процентна ставка та  k-кількість років.*/

    private static void deposit (){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your money to deposit");
        double money = scanner.nextDouble();
        System.out.println("Enter your percent");
        double percent = scanner.nextDouble();
        System.out.println("enter count of years");
        int countOfYears = scanner.nextInt();
        double cash;

        cash = ((money*percent)/100)*countOfYears;
        System.out.println("You will receive "+cash);
    }
}
