package Java_basic.homework08;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ConsoleImpl console = new ConsoleImpl();

        int tmp = 1;

        while (tmp != 0) {

            showOptions();
            int option = scanner.nextInt();
            switch (option) {
                case 0:
                    System.out.println("Enter the month ");
                    String month = scanner.next();
                    boolean chek = console.ifExist(month);
                    if (chek) System.out.println("Month exist " + month);
                    else System.out.println("Incorrectly entered month");
                    break;
                case 1:
                    console.sameSeason();
                    break;
                case 2:
                    console.sameDays();
                    break;
                case 3:
                    console.lessDays();
                    break;
                case 4:
                    console.moreDays();
                    break;
                case 5:
                    console.nextSeason();
                    break;
                case 6:
                    console.previousSeason();
                    break;
                case 7:
                    console.evenDays();
                    break;
                case 8:
                    console.notEvenDays();
                    break;
                case 9:
                    System.out.println("Enter month to check if it have even count of days");
                    String month1= scanner.next();
                    boolean chek1 = console.ifExist(month1);
                    if (chek1) console.evenOrNotDaysInMonth(month1);
                    else System.out.println("Incorrectly entered month");
                    break;
                case 10:
                    tmp = 0;
                    break;
            }
        }


    }

    private static void showOptions() {
        System.out.println("0.Перевірити чи такий місяць існує");
        System.out.println("1.Вивести всі місяці з такою ж порою року");
        System.out.println("2.Вивести всі місяці які мають таку саму кількість днів");
        System.out.println("3.Вивести на екран всі місяці які мають меншу кількість днів");
        System.out.println("4.Вивести на екран всі місяці які мають більшу кількість днів");
        System.out.println("5.Вивести на екран наступну пору року");
        System.out.println("6.Вивести на екран попередню пору року");
        System.out.println("7.Вивести на екран всі місяці які мають парну кількість днів");
        System.out.println("8.Вивести на екран всі місяці які мають непарну кількість днів");
        System.out.println("9.Вивести на екран чи введений з консолі місяць має парну кількість днів");
        System.out.println("10.Вихід!");
        System.out.println("Ведіть номер : ");
    }

}
