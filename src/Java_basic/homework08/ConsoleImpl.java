package Java_basic.homework08;

import Java_basic.homework08.enums.Month;
import Java_basic.homework08.enums.Season;
import Java_basic.homework08.methods.Console;


import java.util.Scanner;

public class ConsoleImpl implements Console {

    Scanner scanner = new Scanner(System.in);
    String month;
    int days;


    @Override
    public void evenOrNotDaysInMonth(String month) {

        for (Month value : Month.values()) {

            if (value.name().equalsIgnoreCase(month)) {

                if (value.getDayInMonth() % 2 == 0) {
                    System.out.println("Month " + value + " have even count of days, days: " + value.getDayInMonth());
                } else
                    System.out.println("Month " + value + " have not even count of days, days: " + value.getDayInMonth());
            }
        }
    }

    @Override
    public boolean ifExist(String month) {
        boolean ifExistMonth = false;
        for (Month value : Month.values()) {

            if (value.name().equalsIgnoreCase(month)){
                ifExistMonth = true;
                System.out.println("Month " + month + " exist");
            }
        }return ifExistMonth;
    }


    @Override
    public void lessDays() {
        System.out.println("Enter the number of days to see which months have less day");
        days= scanner.nextInt();
        for (Month value : Month.values()){
            if (value.getDayInMonth() < days) System.out.println(value.name());
        }
    }

    @Override
    public void moreDays() {
        System.out.println("Enter the number of days to see which months have more day");
        days= scanner.nextInt();
        for (Month value : Month.values()){
            if (value.getDayInMonth() > days) System.out.println(value.name());
        }
    }

    @Override
    public void nextSeason() {
        System.out.println("Enter the season to see the next season");
        month = scanner.next();
        Season currentSeason = Season.valueOf(month.toUpperCase());
        int currentOrdinal = currentSeason.ordinal();
        Season nextSeason;
        if (currentOrdinal == Season.values().length - 1 ){
            nextSeason = Season.values()[0];
        }else nextSeason = Season.values()[currentOrdinal+1];
        System.out.println(nextSeason.name());

    }

    @Override
    public void previousSeason() {
        System.out.println("Enter the season to see the previous season");
        month = scanner.next();
        Season currentSeason = Season.valueOf(month.toUpperCase());
        int currentOrdinal = currentSeason.ordinal();
        Season previousSeason;
        if (currentOrdinal == 0 ){
            previousSeason = Season.values()[3];
        }else previousSeason = Season.values()[currentOrdinal-1];
        System.out.println(previousSeason.name());
    }

    @Override
    public void evenDays() {
        System.out.println("Months with an even number of days ");
        for (Month value : Month.values()) {
            if (value.getDayInMonth() % 2 == 0) System.out.println(value.name());
        }
    }

    @Override
    public void notEvenDays() {
        System.out.println("Months without an even number of days");
        for (Month value : Month.values()) {
            if (value.getDayInMonth() % 2 != 0) System.out.println(value.name());
        }
    }


    @Override
    public void sameDays() {
        System.out.println("Enter the number of days ");
        days= scanner.nextInt();
        for (Month value : Month.values()){
            if (value.getDayInMonth() == days) System.out.println(value.name());
        }
    }

    @Override
    public void sameSeason() {

        System.out.println("Enter the season to see all months in season");
        month = scanner.next();

        for (Month value : Month.values()) {
            if (value.getSeason().name().equalsIgnoreCase(month)) System.out.println(value.name());
        }
    }

}
