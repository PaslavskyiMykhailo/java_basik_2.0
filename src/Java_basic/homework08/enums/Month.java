package Java_basic.homework08.enums;

public enum Month {

    DECEMBER(31,Season.WINTER),
    JANUARY(31,Season.WINTER),
    FEBRUARY(28,Season.WINTER),
    MARCH(31,Season.SPRING),
    APRIL(30,Season.SPRING),
    MAY(31,Season.SPRING),
    JUNE(30,Season.SUMMER),
    JULY(31,Season.SUMMER),
    AUGUST(31,Season.SUMMER),
    SEPTEMBER(30,Season.FALL),
    OCTOBER(31,Season.FALL),
    NOVEMBER(30,Season.FALL);

    private int dayInMonth;
    private Season season;

    Month(int dayInMonth, Season season) {
        this.dayInMonth = dayInMonth;
        this.season = season;
    }

    Month() {
    }

    public int getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

}
