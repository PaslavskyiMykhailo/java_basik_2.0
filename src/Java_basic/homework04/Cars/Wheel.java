package Java_basic.homework04.Cars;

public class Wheel {

    private double wheelDiameter;
    private String wheelColour;

    public Wheel(double wheelDiameter, String wheelColour) {
        this.wheelDiameter = wheelDiameter;
        this.wheelColour = wheelColour;
    }

    public Wheel() {
    }

    public double getWheelDiameter() {
        return wheelDiameter;
    }

    public void setWheelDiameter(double wheelDiameter) {
        this.wheelDiameter = wheelDiameter;
    }

    public String getWheelColour() {
        return wheelColour;
    }

    public void setWheelColour(String wheelColour) {
        this.wheelColour = wheelColour;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "wheelDiameter=" + wheelDiameter +
                ", wheelColour='" + wheelColour + '\'' +
                '}';
    }
}
