package Java_basic.homework04.Cars;

public class CarBody {

    private String bodyColour;
    private int bodySize;

    public CarBody(String bodyColour, int bodySize) {
        this.bodyColour = bodyColour;
        this.bodySize = bodySize;
    }

    public CarBody() {
    }

    public String getBodyColour() {
        return bodyColour;
    }

    public void setBodyColour(String bodyColour) {
        this.bodyColour = bodyColour;
    }

    public int getBodySize() {
        return bodySize;
    }

    public void setBodySize(int bodySize) {
        this.bodySize = bodySize;
    }

    @Override
    public String toString() {
        return "CarBody{" +
                "bodyColour='" + bodyColour + '\'' +
                ", bodySize=" + bodySize +
                '}';
    }
}
