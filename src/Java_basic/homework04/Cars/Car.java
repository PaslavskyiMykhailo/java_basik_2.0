package Java_basic.homework04.Cars;

public class Car {

    private double carPrice;
    private int countOfCars;
    private CarBody carBody;
    private SteeringWheel steeringWheel;
    private Wheel wheel;

    public Car(double carPrice, int countOfCars, CarBody carBody, SteeringWheel steeringWheel, Wheel wheel) {
        this.carPrice = carPrice;
        this.countOfCars = countOfCars;
        this.carBody = carBody;
        this.steeringWheel = steeringWheel;
        this.wheel = wheel;
    }

    public void changeWheelSize (double newSize){
        wheel.setWheelDiameter(newSize);

    }
    public void changeCarBodyColour(String newColour){
        carBody.setBodyColour(newColour);
    }



    public double getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(double carPrice) {
        this.carPrice = carPrice;
    }

    public int getCountOfCars() {
        return countOfCars;
    }

    public void setCountOfCars(int countOfCars) {
        this.countOfCars = countOfCars;
    }

    public CarBody getCarBody() {
        return carBody;
    }

    public void setCarBody(CarBody carBody) {
        this.carBody = carBody;
    }

    public SteeringWheel getSteeringWheel() {
        return steeringWheel;
    }

    public void setSteeringWheel(SteeringWheel steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carPrice=" + carPrice +
                ", countOfCars=" + countOfCars +
                ", carBody=" + carBody +
                ", steeringWheel=" + steeringWheel +
                ", wheel=" + wheel +
                '}';
    }

}
