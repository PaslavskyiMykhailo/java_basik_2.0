package Java_basic.homework04.Cars;

public class SteeringWheel {
    private int sizeOfWheel;
    private String steeringColour;

    public SteeringWheel(int sizeOfWheel, String steeringColour) {
        this.sizeOfWheel = sizeOfWheel;
        this.steeringColour = steeringColour;
    }

    public SteeringWheel() {
    }

    public int getSizeOfWheel() {
        return sizeOfWheel;
    }

    public void setSizeOfWheel(int sizeOfWheel) {
        this.sizeOfWheel = sizeOfWheel;
    }

    public String getSteeringColour() {
        return steeringColour;
    }

    public void setSteeringColour(String steeringColour) {
        this.steeringColour = steeringColour;
    }

    @Override
    public String toString() {
        return "SteeringWheel{" +
                "sizeOfWheel=" + sizeOfWheel +
                ", steeringColour='" + steeringColour + '\'' +
                '}';
    }
}
