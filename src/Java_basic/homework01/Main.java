package Java_basic.homework01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // homeOne(5, 2, 1);
        // diary();
        // firstFiftyFive();
        // ninetyZero();
        // closeNumber(9, -9);
        //array();

    }

    private static void array() {
        int[] arr = new int[20];
        int min = 0;
        int max = 0;

        for (int i = 0; i < 20; i++) {
            arr[i] = (int) (Math.random() * (200 + 1)) - 100;
        }
        for (int i = 0; i < 20; i++) {
            System.out.println(arr[i]);
        }
        for (int i = 0; i < 20; i++) {
            if (min > arr[i]) min = arr[i];
            if (max < arr[i]) max = arr[i];
        }
        System.out.println("min "+ min + " max " +max);
    }

    private static void homeOne(int a, int b, int c) {
        double D = Math.pow(b, 2) - (4 * a * c);
        System.out.println("D = " + D);
        if (D < 0) {
            System.out.println("Equation has no answer");
        } else if (D == 0) {
            int x = -b / (2 * a);
            System.out.println("Answer is : x = " + x);
        } else {
            double x1 = (-b + Math.sqrt(D)) / (2 * a);
            double x2 = (-b - Math.sqrt(D)) / (2 * a);
            System.out.println("there are two answers : x1 = " + x1 + " x2 = " + x2);
        }
    }

    private static void diary() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write a day to see what to do");
        String day = scanner.next();

        switch (day) {
            case "Monday": {
                System.out.println("todo something on monday");
            }
            break;
            case "Tuesday": {
                System.out.println("todo something on tuesday");
            }
            break;
            case "Wednesday": {
                System.out.println("todo something on wednesday");
            }
            break;
            case "Thursday": {
                System.out.println("todo something on thursday");
            }
            break;
            case "Friday": {
                System.out.println("todo something on friday");
            }
            break;
            case "Saturday": {
                System.out.println("todo something on saturday");
            }
            break;
            case "Sunday": {
                System.out.println("Relax");
            }
            break;
            default: {
                System.out.println("Write correct name of day");
            }
        }
    }

    private static void firstFiftyFive() {
        int[] mass = new int[55];
        int a = 1;
        for (int i = 0; i < mass.length; i++) {
            mass[i] = a;
            a = a + 2;
        }
        for (int i = 0; i < mass.length; i++) {
            System.out.println(mass[i]);
        }
    }

    private static void ninetyZero() {
        for (int i = 90; i >= 0; i = i - 5) {
            System.out.println(i);
        }
    }

    private static void closeNumber(int m, int n) {
        int b = 10 - m;
        System.out.println(b);
        int c = 10 - n;
        System.out.println(c);
        if (b < c) {
            System.out.println("The closest number is : " + m);
        } else System.out.println("The closest number is :" + n);
    }
}
