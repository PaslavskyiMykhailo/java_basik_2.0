package Java_basic.homework03;

public class Main03 {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(15.5, 10.3);
        System.out.println("Площа прямокутника = " + rectangle.square());
        System.out.println("Периметр прямокутника = " + rectangle.perimeter());

        Circle circle = new Circle(12.3, 24.6);
        System.out.println("Довжина кола = " + circle.longOfCircle());
        System.out.println("Площа кола = " + circle.squareOfCircle());
    }
}
