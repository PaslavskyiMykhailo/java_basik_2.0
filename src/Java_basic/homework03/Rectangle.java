package Java_basic.homework03;

public class Rectangle {
    private double width;
    private double high;

    public double square() {
        return width * high;
    }

    public double perimeter() {
        return (2 * width) + (2 * high);
    }

    public Rectangle(double width, double high) {
        this.width = width;
        this.high = high;
    }

    public Rectangle() {
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", high=" + high +
                '}';
    }
}
