package Java_basic.homework03;

public class Circle {
    private double radius;
    private double diameter;

    public double longOfCircle() {
        return 2 * Math.PI * radius;
    }

    public double squareOfCircle() {
        return (Math.PI * Math.pow(diameter, 2)) / 4;
    }

    public Circle() {
    }

    public Circle(double radius, double diameter) {
        this.radius = radius;
        this.diameter = diameter;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", diameter=" + diameter +
                '}';
    }
}
